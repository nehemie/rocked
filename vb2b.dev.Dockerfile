FROM debian:testing
LABEL version="0.0.9001"

MAINTAINER "Nehemie" nehemie.strupler+docker@posteo.net

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    dpkg \
    make \
    wget \
    locales

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen en_US.utf8 \
  && /usr/sbin/update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

## install R, needed debian package and set a default CRAN repo
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    gdal-bin \
    libgdal-dev \
    libproj-dev \
    libgeos-dev \
    libgeos++-dev \
    libudunits2-dev \
    r-base \
    r-base-dev \
    r-recommended \
    r-cran-backports \
    r-cran-data.table \
    r-cran-dplyr \
    r-cran-ggplot2 \
    r-cran-gtools \
    r-cran-hmisc \
    r-cran-htmlwidgets \
    r-cran-jsonlite \
    r-cran-latticeextra \
    r-cran-maptools \
    r-cran-knitr \
    r-cran-plyr \
    r-cran-raster \
    r-cran-reshape \
    r-cran-spatstat \
    r-cran-xtable \
    r-cran-zoo \
    && echo 'options(repos = c(CRAN = "https://cran.rstudio.com/"), download.file.method = "libcurl")' >> /etc/R/Rprofile.site \
    && Rscript -e "install.packages(c('rgdal', 'rgeos'))"

#  Install Inkscape and LaTeX
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    inkscape \
    texlive-base \
    texlive-fonts-recommended \
    texlive-xetex \
    biber \
    texlive-bibtex-extra \
    texlive-lang-french \
    texlive-lang-german \
    texlive-lang-italian \
    texlive-latex-extra \
    texlive-fonts-extra \
    texlive-science \
    texlive-plain-generic \
    fonts-linuxlibertine

#  Install git (to clone)
RUN apt-get update \
  && apt-get install -y --no-install-recommends git

#  Install rename (to move figures)
RUN apt-get update \
  && apt-get install -y --no-install-recommends rename

