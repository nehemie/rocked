all: build-all push-all

build-all: build-dev build-sid

build-dev:
	docker build -t nehemie/vb2b:dev ./

build-sid:
	docker build -t nehemie/vb2b:sid sid/

push-all: push-dev push-sid

push-dev:
	docker push "$$CI_REGISTRY_IMAGE"nehemie/vb2b:dev

push-sid:
	docker push "$$CI_REGISTRY_IMAGE"nehemie/vb2b:sid
