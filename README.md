## About this repository

This repository contains `Dockerfile`s and a `.gitlab.ci` pipeline to
build and publish the image on https://index.docker.io/nehemie
