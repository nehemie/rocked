# rocked      |      2021-12-13 |
# == website.master.Dockerfile ==

## ---------------------------

FROM debian:testing
LABEL version="0.0.1"

LABEL image.author.email="nehemie.strupler+docker@posteo.net"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    dpkg \
    make \
    wget \
    curl \
    unzip \
    locales

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen en_US.utf8 \
  && /usr/sbin/update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

## install R, needed debian package and set a default CRAN repo
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    g++ \
    gdal-bin \
    libgdal-dev \
    libproj-dev \
    libgeos-dev \
    libgeos++-dev \
    libudunits2-dev \
    imagemagick \
    libmagick++-dev \
    libv8-dev \
    texlive-base \
    texlive-bibtex-extra \
    texlive-binaries \
    texlive-latex-base \
    texlive-fonts-recommended \
    texlive-fonts-extra \
    texlive-lang-french \
    texlive-lang-german \
    texlive-lang-italian \
    texlive-latex-extra \
    texlive-pictures \
    texlive-plain-generic \
    texlive-publishers \
    texlive-science \
    texlive-xetex \
    fonts-linuxlibertine \
    lmodern \
    biber \
    r-base \
    r-base-dev \
    r-recommended \
    r-cran-backports \
    r-cran-base64enc \
    r-cran-bh \
    r-cran-biocmanager \
    r-bioc-complexheatmap \
    r-bioc-iranges \
    r-bioc-graph \
    r-cran-gridbase \
    r-cran-gridextra \
    r-cran-gridgraphics \
    r-cran-gridtext \
    r-cran-classint \
    r-cran-covr \
    r-cran-cowplot \
    r-cran-data.table \
    r-cran-devtools \
    r-cran-dplyr \
    r-cran-fs \
    r-cran-ggplot2 \
    r-cran-gtools \
    r-cran-hmisc \
    r-cran-htmltools \
    r-cran-htmlwidgets \
    r-cran-jsonlite \
    r-cran-latticeextra \
    r-cran-lazyeval \
    r-cran-lubridate \
    r-cran-magick \
    r-cran-magrittr \
#    r-cran-maptools \
    r-cran-nlp \
    r-cran-knitr \
    r-cran-patchwork \
    r-cran-plyr \
    r-cran-rmarkdown \
    r-cran-raster \
    r-cran-reshape \
    r-cran-reshape2 \
    r-cran-rex \
    r-cran-rlang \
    r-cran-rcpp \
#    r-cran-spatstat \
    r-cran-units \
    r-cran-rmarkdown \
    r-cran-rcolorbrewer \
    r-cran-ragg \
    r-cran-raster \
#    r-cran-rgdal \
    r-cran-rvest \
    r-cran-s2 \
    r-cran-selectr \
    r-cran-sf \
    r-cran-slam \
    r-cran-snowballc \
    r-cran-sp \
    r-cran-textshaping \
    r-cran-tm \
    r-cran-wk \
    r-cran-wordcloud \
    r-cran-xfun \
    r-cran-xml \
    r-cran-xts \
    r-cran-xtable \
    r-cran-wordcloud \
    r-cran-zoo \
    git \
    pandoc \
    rename \
    ruby \
    ruby-bundler \
    jekyll \
    && echo 'options(repos = c(CRAN = "https://cran.rstudio.com/"), download.file.method = "libcurl")' >> /etc/R/Rprofile.site \
    && Rscript -e "install.packages(c('cffr'))" \
    && Rscript -e "install.packages(c('rgeos', 'rnaturalearth', 'rnaturalearthdata'))" \
    && Rscript -e "install.packages(c('sf'))" \
    && Rscript -e "install.packages(c('libgeos', 'geos'))" \
    && Rscript -e "install.packages(c('osmdata'))" \
    && Rscript -e "install.packages(c('pkgdown','blogdown'))" \
    && Rscript -e "BiocManager::install('Rgraphviz')"

## install html validator in ./vnu
RUN wget https://github.com/validator/validator/releases/download/latest/vnu.linux.zip \
  && unzip vnu.linux.zip -d vnu

## Install quarto
RUN curl -LO https://quarto.org/download/latest/quarto-linux-amd64.deb
RUN dpkg -i quarto-linux-amd64.deb

RUN echo "LIST OF INSTALLED PACKAGE" >> os_config.txt \
  && echo "-------------------------"  >> os_config.txt \
  && dpkg -l   >> os_config.txt \
  && echo ""  >> os_config.txt \
  && echo "CPU architecture"   >> os_config.txt \
  && echo "-------------------------"  >> os_config.txt \
  && lscpu >> os_config.txt \
  && echo ""  >> os_config.txt \
  && echo "LINUX DIST AND VERSION"  >> os_config.txt \
  && echo "-------------------------"  >> os_config.txt \
  && cat /etc/os-release  >> os_config.txt \
  && cat os_config.txt

# bye-bye....................|
