#!/bin/sh
echo "$IMAGE_PROJECT"
echo "$IMAGE_TAG"
docker build -t "$CI_REGISTRY_USER/$IMAGE_PROJECT:$IMAGE_TAG" -f "$IMAGE_PROJECT.$IMAGE_TAG.Dockerfile" ./
docker push "$CI_REGISTRY_USER/$IMAGE_PROJECT:$IMAGE_TAG"
