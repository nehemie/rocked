# rocked      |      2021-12-13 |
# == static-website.master.Dockerfile ==

## ---------------------------

FROM debian:testing
LABEL version="0.0.1"

LABEL image.author.email="nehemie.strupler+docker@posteo.net"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    dpkg \
    make \
    wget \
    curl \
    unzip \
    locales

RUN echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen en_GB.utf8 \
  && /usr/sbin/update-locale LANG=en_GB.UTF-8

ENV LC_ALL=en_GB.UTF-8
ENV LANG=en_GB.UTF-8

## install R, needed debian package and set a default CRAN repo
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    g++ \
    gdal-bin \
    libgdal-dev \
    libproj-dev \
    libgeos-dev \
    libgeos++-dev \
    libudunits2-dev \
    imagemagick \
    libmagick++-dev \
    libv8-dev \
    r-base \
    r-base-dev \
    r-recommended \
    r-cran-classint \
    r-cran-cowplot \
    r-cran-data.table \
    r-cran-devtools \
    r-cran-dplyr \
    r-cran-fs \
    r-cran-geosphere \
    r-cran-ggplot2 \
    r-cran-hmisc \
    r-cran-htmltools \
    r-cran-htmlwidgets \
    r-cran-jsonlite \
    r-cran-latticeextra \
    r-cran-lazyeval \
    r-cran-lubridate \
    r-cran-magick \
    r-cran-magrittr \
    r-cran-ncdfgeom \
    r-cran-knitr \
    r-cran-patchwork \
    rasterio \
    r-cran-rmarkdown \
    r-cran-readr \
    r-cran-reshape \
    r-cran-reshape2 \
    r-cran-rex \
    r-cran-rlang \
    r-cran-rcpp \
    r-cran-units \
    r-cran-rmarkdown \
    r-cran-rcolorbrewer \
    r-cran-ragg \
    r-cran-rvest \
    r-cran-s2 \
    r-cran-sf \
    r-cran-terra \
    r-cran-zoo \
    r-cran-rnaturalearth \
    r-cran-rnaturalearthdata \
    r-cran-spdata \
    git \
    pandoc \
    rename \
    && echo 'options(repos = c(CRAN = "https://cran.rstudio.com/"), download.file.method = "libcurl")' >> /etc/R/Rprofile.site \
    && Rscript -e "install.packages(c('osmdata'))" \
    && Rscript -e "install.packages(c('cffr'))"

## install html validator in ./vnu
RUN wget https://github.com/validator/validator/releases/download/latest/vnu.linux.zip \
  && unzip vnu.linux.zip -d vnu

## Install quarto
RUN curl -LO https://quarto.org/download/latest/quarto-linux-amd64.deb
RUN dpkg -i quarto-linux-amd64.deb

RUN echo "LIST OF INSTALLED PACKAGE" >> os_config.txt \
  && echo "-------------------------"  >> os_config.txt \
  && dpkg -l   >> os_config.txt \
  && echo ""  >> os_config.txt \
  && echo "CPU architecture"   >> os_config.txt \
  && echo "-------------------------"  >> os_config.txt \
  && lscpu >> os_config.txt \
  && echo ""  >> os_config.txt \
  && echo "LINUX DIST AND VERSION"  >> os_config.txt \
  && echo "-------------------------"  >> os_config.txt \
  && cat /etc/os-release  >> os_config.txt \
  && cat os_config.txt

# bye-bye....................|
